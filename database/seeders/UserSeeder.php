<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'code' => 'ADM',
            'name' =>'Admin',
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'code' => 'KSR',
            'name' =>'Kasir',
        ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@app.com',
            'password' => bcrypt('123456'),
            'role_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Kasir',
            'email' => 'kasir@app.com',
            'password' => bcrypt('123456'),
            'role_id' => 2,
        ]);
    }
}
