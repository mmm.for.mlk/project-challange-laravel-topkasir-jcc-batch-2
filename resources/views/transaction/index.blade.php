@extends('layouts.master')

@section('name')
    Daftar Transaksi
@endsection

@section('content')
<form action="{{ url('transaction') }}" method="POST">
    @csrf
    <button type="submit" class="btn btn-primary">Buat Transaksi Baru</button>
</form>

<div class="container-fluid">
    <div class="card position-relative">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Table Transaksi</h6>
        </div>
        <div class="card-body">
            <table class="table form">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Waktu</th>
                    <th scope="col">Total Harga</th>
                    <th scope="col">Detail</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $index => $item) 
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td>{{ $item->created_at }}</td>
                        <td align="right">{{ $item->total_harga }}</td>
                        <td>
                            <a href="{{ url('purchase/' . $item->id) }}" class="btn btn-primary btn-sm">Tambahkan</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection