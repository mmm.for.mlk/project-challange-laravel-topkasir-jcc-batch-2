<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    public function postlogin(Request $request) {
        $credentials = $request->only('email', 'password');
 
        if (Auth::attempt($credentials)) {
            return redirect('transaction');
        }

        return redirect()->back();
    }

    public function postLogout() {
        Auth::logout();

        return redirect('login');
    }
}
