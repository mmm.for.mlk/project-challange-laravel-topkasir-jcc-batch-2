<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasterBarang;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use Barryvdh\DomPDF\Facade\Pdf;

class TransaksiPembelianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($transaksi_pembelian_id)
    {
        $barang = MasterBarang::pluck('nama_barang', 'id');
        $id_barang = request()->get('namabarang');
        $selected_barang = MasterBarang::firstWhere('id', $id_barang);
        $list_barang = TransaksiPembelianBarang::with(['TransaksiPembelian','MasterBarang'])->where('transaksi_pembelian_id', $transaksi_pembelian_id)->get();
        $transaksi = TransaksiPembelian::find($transaksi_pembelian_id);

        return view('purchase', compact('barang', 'selected_barang', 'list_barang', 'id_barang', 'transaksi_pembelian_id', 'transaksi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $transaksi_pembelian_id)
    {
        $jumlah_barang = $request->jumlahbarang;
        $harga_satuan = $request->hargasatuan;
        $total_harga = $jumlah_barang * $harga_satuan;

        $barang = new TransaksiPembelianBarang;
        $barang->transaksi_pembelian_id = $transaksi_pembelian_id;
        $barang->master_barang_id = $request->namabarang;
        $barang->jumlah = $jumlah_barang;
        $barang->harga_satuan = $harga_satuan;
        $barang->save();

        $pembelian = TransaksiPembelian::find($transaksi_pembelian_id);
        $pembelian->total_harga = ($pembelian->total_harga + $total_harga);
        $pembelian->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($transaksi_pembelian_id, $id)
    {
        $data = TransaksiPembelianBarang::find($id);

        $total = ($data->jumlah * $data->harga_satuan);
        
        $data->delete();

        $pembelian = TransaksiPembelian::find($transaksi_pembelian_id);
        $pembelian->total_harga = ($pembelian->total_harga - $total);
        $pembelian->save();

        return redirect()->back();
    }

    public function printpdf($transaksi_pembelian_id)
    {
        $data['data'] = TransaksiPembelian::find($transaksi_pembelian_id)->load('TransaksiPembelianBarang');
        // dd($data);
        $pdf = PDF::loadView('transaction.print-pdf', $data);
        return $pdf->stream('invoice.pdf');
    }
}
