<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TransaksiPembelianController;
use App\Http\Controllers\TransaksiPembelianBarangController;
use App\Http\Controllers\MasterBarangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('login');
});

Route::resource('/transaction', TransaksiPembelianController::class);

Route::get('/purchase/{transaksi_pembelian_id}', [TransaksiPembelianBarangController::class, 'index']);
Route::get('/purchase/{transaksi_pembelian_id}/pdf', [TransaksiPembelianBarangController::class, 'printpdf']);
Route::post('/purchase/{transaksi_pembelian_id}', [TransaksiPembelianBarangController::class, 'store']);
Route::delete('/purchase/{transaksi_pembelian_id}/{id}', [TransaksiPembelianBarangController::class, 'destroy']);

Route::get('/login', function() {
    return view('login');
});

Route::post('/login', [LoginController::class, 'postlogin']);
Route::get('/logout', [LoginController::class, 'postLogout']);

Route::get('/master-barang', [MasterBarangController::class, 'index']);